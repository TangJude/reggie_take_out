package com.itheima.reggie.common;

/**
 * 基于ThreadLocal封装工具类，用户保存和获取当前登录用户id
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal;

    /**
     * 设置值
     * @param id
     */
    public static void setCurrentId(Long id){
        if (threadLocal == null){
            threadLocal = new ThreadLocal<>();
        }
        threadLocal.set(id);
    }

    /**
     * 获取值
     * @return
     */
    public static Long getCurrentId(){
        if (threadLocal == null){
            threadLocal = new ThreadLocal<>();
        }
        return threadLocal.get();
    }
}